/*1. Написати програму, яка розраховує суму двох дійсних чисел введених у
	консоль. Результат вивести у консоль у форматованому вигляді з одним
	знаком після коми.*/
import java.util.Scanner;
public class Task1 {
	public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
       
            System.out.println("Введіть перше число:");
            double a = sc.nextDouble();
            System.out.println("Введіть друге число:");
            double b = sc.nextDouble();
            double sum = a+b;
            System.out.println("Сумма = "+sum);
        
    }
  }