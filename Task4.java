/*4. Написати програму, яка обчислює суму окремих розрядів цілого числа.
Результат вивести у консоль.*/

import java.util.Scanner;

public class Task4 {
  public static void main(String args[]) {
    Scanner Obj = new Scanner(System.in);
    System.out.println("Введіть ціле число:");  
    int n = Obj.nextInt();
    
    int summ = 0;
    for (;n > 0; n /= 10)
      summ += n%10;
    
    System.out.println("Результат: "+summ);  
  }
}