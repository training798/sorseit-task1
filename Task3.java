/*3. Написати програму, що визначає найбільший спільний дільник двох цілих
чисел. Результат вивести у консоль.*/

import java.util.Scanner;
public class Task3 {
  public static void main(String args[]) {
    Scanner Obj = new Scanner(System.in);
    System.out.println("Введіть перше число:");  
    int number1 = Obj.nextInt();
    System.out.println("Введіть друге число:");
    int number2 = Obj.nextInt();
    
    if(number1<number2) {
      int number = number1;
      number1 = number2;
      number2 = number;
    }
    int res;
    do {
      res = number1%number2;
      number1 = number2;
      number2 = res;
    }
    while(res!=0);
    
    System.out.println("Найбільший спільний дільник: "+number1);
    }
    
  }