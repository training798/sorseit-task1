/*5. Написати програму, яка визначить та виведе усі прості числа в діапазоні від
1 до n (число n вводиться у консоль).*/
import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		System.out.println("Введіть число:");
		Scanner numer = new Scanner(System.in);
		int n = numer.nextInt();	
		boolean a = true;
		
		for (int k = 2; k <= n; k++) {
			for (int i = 2; i < k; i++) {
				if (k % i == 0) {
					a = false;
					break;
				}
			}
		
			if (a) System.out.println(k);
			else a = true;	
		}
	}
}

